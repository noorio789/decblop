Description: 
---------------
This is the third assignment in Computer vision course. It is about developing a labeling algorithms to extract traffic signs given by a 25 image. They are a yellow and red signs :)	

Notice:
-------------
For Assignment Specifications and Requirements, Please Open Assginment03.pdf.
For the Results and Report, Open the PDF of the Report.

Screenshot
------------------------------------

![Alt text](https://bytebucket.org/noorio789/decblop/raw/1f1c223a4b4847f4e09e70729c552899914e1d28/Signs.png "The Result of 25 Sign Dectected with a blue Rectangle Surrounds them")