

#include <bits/stdc++.h>
#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/core/matx.hpp"
using namespace std;
using namespace cv;

/*Implmented Function with errors*/

//int maxP,maxB;
// void blopping(Mat src, Mat blopimg)
// {
//   int count = 0,dec = 0;
//   int l = 0,u1 = 0 ,u2 = 0 ,u3 = 0;
//   maxB = 0;
//   set<int> s;
//   unordered_map<int,int> m;
//   m.clear();
//
//   for (int i = 0; i < src.rows; i++)
//     for(int j = 0; j < src.cols; j++)
//       {
//          blopimg.at<uchar>(i,j) = 0;
//          blopimg.at<Vec3b>(i,j) = Vec3b{0,0,0};
//
//       }
//
//   for(int i = 0; i < src.rows; i++)
//     {
//        m[i] = i;
//        cout << "Fe eh" << endl;
//     }
//
//
//   for (int i = 0; i < src.rows; i++)
//     for(int j = 0; j < src.cols; j++)
//     {
//
//       l = 0;
//       u1 = 0;
//       u2 = 0;
//       u3 = 0;
//
//         if(cvRound(src.at<uchar>(i,j)) == 255)
//         {
//           if( i != 0 && i < src.rows - 1)
//           {
//             if(j != 0 && j < src.cols - 1)
//             {
//                 if(blopimg.at<uchar>(i - 1,j - 1) != 0)
//                   u1 = blopimg.at<uchar>(i - 1,j - 1);
//
//                 if(blopimg.at<uchar>(i - 1,j) != 0)
//                   u2 = blopimg.at<uchar>(i - 1,j);
//
//                 if(blopimg.at<uchar>(i -1,j+1) != 0)
//                   u3 = blopimg.at<uchar>(i - 1,j+1);
//
//                 if(blopimg.at<uchar>(i ,j - 1) != 0)
//                   l = blopimg.at<uchar>(i ,j - 1);
//             }
//             else
//             {
//                 if(blopimg.at<uchar>(i - 1 ,j) != 0)
//                   u2 = blopimg.at<uchar>(i - 1,j);
//
//                 if(blopimg.at<uchar>(i - 1,j + 1) != 0)
//                   u3 = blopimg.at<uchar>(i - 1,j + 1);
//             }
//           }
//           else
//           {
//             if(j != 0 && j < src.cols - 1)
//             {
//                 if(blopimg.at<uchar>(i ,j - 1) != 0)
//                   l = blopimg.at<uchar>(i ,j - 1);
//             }
//           }
//
//           if( l != 0 || u1 != 0 || u2 != 0 || u3 != 0)
//           {
//             s.clear();
//             s.insert(l);
//             s.insert(u1);
//             s.insert(u2);
//             s.insert(u3);
//             s.erase(0);
//
//             for(set<int>::iterator it = s.begin(); it != s.end(); it++)
//                 m[*it] = *s.begin();
//
//               blopimg.at<uchar>(i,j) = m[*s.begin()];
//
//           }
//           else
//           {
//             count++;
//             blopimg.at<uchar>(i,j) = count;
//           }
//           }
//     }
//     for (int i = 0; i < src.rows; i++)
//       for(int j = 0; j < src.cols; j++)
//           blopimg.at<uchar>(i,j) = m[blopimg.at<uchar>(i,j)];
//
//           //cout << "asda :Asdasd" << m[24];
//           count = 0;
//           s.clear();
//
//       map<int,int> mCount;
//
//       for (int i = 0; i < blopimg.rows; i++)
//         for(int j = 0; j < blopimg.cols; j++)
//             s.insert(cvRound(blopimg.at<uchar>(i,j)));
//
//       s.erase(0);
//
//       for(set<int>::iterator it = s.begin(); it != s.end(); ++it)
//         for (int i = 0; i < blopimg.rows; i++)
//           for(int j = 0; j < blopimg.cols; j++)
//               if(cvRound(blopimg.at<uchar>(i,j)) == *it)
//                   mCount[*it]++;
//
//       for(map<int,int>::iterator it = mCount.begin(); it != mCount.end(); ++it)
//         {
//           cout << " The Blop: " << it-> first << " Votes: " << it -> second << endl;
//           if(it->second > maxP)
//           {
//             maxB = it->first;
//             maxP = it -> second;}
//           }
//           count = s.size();
//
//
//     cout << "Number of blops: " << count << endl;
//   return;
// }


int main()
{
  Mat frame,frameThres,Rframe,Yframe,NRframe;
  int x = 1;
  while(x < 26)
  {
    string path;
    if(x <= 9)
    path = "./Am_Rojo000" + std::to_string(x) + ".jpg";
    else
    path = "./Am_Rojo00" + std::to_string(x) + ".jpg";
  frame = imread(path,CV_LOAD_IMAGE_COLOR);

  resize(frame,frame,Size(),0.25,0.25,INTER_NEAREST);

  Mat HSVframe;

  cvtColor(frame,HSVframe,CV_BGR2HSV);

  cout << frame.size() << endl;

  //For the Red Ranges
  inRange(HSVframe,Scalar(0,30,30),Scalar(12,255,255),Rframe);
  inRange(HSVframe,Scalar(170,30,30),Scalar(180,255,255),NRframe);

  bitwise_or(Rframe,NRframe,Rframe);
  // For the Yellow Ranges
  inRange(HSVframe,Scalar(17,150,30),Scalar(27,200,50),Yframe);

  Rframe.copyTo(frameThres);

  for (int i = 0; i < Yframe.rows; i++)
    for(int j = 0; j < Yframe.cols; j++)
      if(cvRound(Yframe.at<uchar>(i,j)) == 255)
          frameThres.at<uchar>(i,j) = 255;

  Mat Blopframe;

  frameThres.copyTo(Blopframe);

  Mat stat,cent;

  connectedComponentsWithStats(frameThres,Blopframe,stat,cent,8);
//  blopping(Blopframe);

  std::cout << Blopframe << '\n';
  std::cout << cent << '\n';
//  Mat res = Mat(frame.rows,frame.col,frame.type());

  int cArea = 0,x0 = frameThres.rows, xf = 0, yf = 0, y0 = frameThres.cols;
  vector<Vec4i> rect;
  int wArea;
  int mc = 0;
  bool first = true;
  for(int i = 0; i < stat.rows; i++)
  {
     int x = stat.at<int>(Point(0, i));
     int y = stat.at<int>(Point(1, i));
     int w = stat.at<int>(Point(2, i));
     int h = stat.at<int>(Point(3, i));
     int area = stat.at<int>(Point(4,i));

     if(float(w)/float(h) < 1.2 && float(h)/float(w) < 1.2 && w < frameThres.cols && area > 30 && mc < 2){
     if(area > cArea && first)
     {
       first = false;
       mc++;
       wArea = area;
       Vec4i r;
       cArea = area;
       r[0] = x;
       r[1] = y;
       r[2] = w;
       r[3] = h;
       rect.push_back(r);
      //  cout << " Data Cooredinates: " << x0 << ' ' << y0 << ' ' << xf << ' ' << yf << endl;
      //  cv::rectangle(frame,Rect(x0,y0,xf,yf),Scalar(0,0,255));
     }
     else if(abs(wArea - area) < 10 && area > cArea)
     {
         mc++;
         wArea = area;
         Vec4i r;
         cArea = area;
         r[0] = x;
         r[1] = y;
         r[2] = w;
         r[3] = h;
         rect.push_back(r);
        //  cout << " Data Cooredinates: " << x0 << ' ' << y0 << ' ' << xf << ' ' << yf << endl;
        //  cv::rectangle(frame,Rect(x0,y0,xf,yf),Scalar(0,0,255));
       }
     }
   }

  for(int i = 0; i < rect.size(); i++)
  {
    cout << " Data Cooredinates: " << rect[0] << ' ' << rect[1] << ' ' << rect[2] << ' ' << rect[3] << endl;
    cout << " Area: " << cArea << endl;
    cv::rectangle(frame,Rect(rect[i][0],rect[i][1],rect[i][2],rect[i][3]),Scalar(255,0,0));
  }

  imshow( std::to_string(x) , frame);
  imshow( "Demo", frameThres);
  x++;
  }

  waitKey(0);
  return 0;
}
